import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'home',
            Component:Home
        },
        {
            path: '/get-api-component',
            name: 'GetApiComponent',
            Component: () => import('./views/GetApiComponent.vue')
        },
        {
            path: '/post-api-components',
            name: 'PostApiComponents',
            Component: () => import('./views/PostApiComponents.vue')
        }

    ]
})