 import Vue from 'vue'
 import Vuex from 'vuex'

 Vue.use(Vuex)

 export default new Vuex.store({
     state: {
        name: null
     },
     mutation: {
        setName (state, name) {
            state.name = name
        }
     },
     actions: {
 
     },
     getters: {
        nameStore: state =>state.name
     }
 })